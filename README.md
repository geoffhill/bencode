# bencode

Various snippets that I wanted to share with Ben.

### Copyright

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

See COPYING for details.
