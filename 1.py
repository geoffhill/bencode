# Copyright 2016 Geoff Hill.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Code to count the number of inversions in a list of numbers."""

import sys
import unittest

def merge(left, right):
    """Combine two sorted lists to produce a new list, including duplicates."""
    newlist = []
    leftidx, rightidx = 0, 0
    for _ in range(len(left) + len(right)):
        take_from = left
        if leftidx >= len(left):
            take_from = right
        elif rightidx < len(right) and right[rightidx] < left[leftidx]:
            take_from = right
        if take_from is left:
            newlist.append(left[leftidx])
            leftidx += 1
        else:
            newlist.append(right[rightidx])
            rightidx += 1
    return newlist

def count_lt(item, lst):
    """Given an item and a sorted list, determine how many elements < item.

    Uses binary search by comparing the list midpoint to the item and cutting
    the considered range in half at each step.

    """
    start = 0
    end = len(lst)
    while start < end:
        mid = (start + end) / 2
        if lst[mid] < item:
            start = mid + 1
        else:
            end = mid
    return start

def count_inversions(lst, others=None):
    """Count the number of unsorted pairs present in a list using recursion."""
    if others is None:
        others = []
    if len(lst) == 0:
        return 0
    elif len(lst) == 1:
        return count_lt(lst[0], others)
    else:
        mid = len(lst) / 2
        lhs = lst[:mid]
        rhs = lst[mid:]
        cumulative = merge(others, sorted(rhs))
        return count_inversions(lhs, cumulative) + count_inversions(rhs, others)


class TestInversions(unittest.TestCase):
    """Test the functions defined in this file."""

    def test_merge(self):
        """Test correctness of the merge() function."""
        self.assertEqual(merge([], []), [])
        self.assertEqual(merge([1], []), [1])
        self.assertEqual(merge([], [2]), [2])
        self.assertEqual(merge([3], [3]), [3, 3])
        self.assertEqual(merge([3, 4], [3, 4]), [3, 3, 4, 4])
        self.assertEqual(merge([1, 5], [2, 3, 7]), [1, 2, 3, 5, 7])
        self.assertEqual(merge([6], [1, 4, 5]), [1, 4, 5, 6])
        self.assertEqual(merge([2, 4], [1, 3, 5]), [1, 2, 3, 4, 5])
        self.assertEqual(merge([], [1, 2, 3, 8]), [1, 2, 3, 8])
        self.assertEqual(merge([5, 7, 9], []), [5, 7, 9])
        self.assertEqual(merge([1, 3, 5, 7], [2, 4, 6]), [1, 2, 3, 4, 5, 6, 7])

    def test_count_lt(self):
        """Test correctness of the count_lt() function."""
        self.assertEqual(count_lt(5, []), 0)
        self.assertEqual(count_lt(5, [1]), 1)
        self.assertEqual(count_lt(5, [5]), 0)
        self.assertEqual(count_lt(5, [7]), 0)
        self.assertEqual(count_lt(8, [7, 9]), 1)
        self.assertEqual(count_lt(5, [1, 3, 4]), 3)
        self.assertEqual(count_lt(5, [1, 3, 5]), 2)
        self.assertEqual(count_lt(5, [1, 3, 6]), 2)
        self.assertEqual(count_lt(5, [6, 7, 8]), 0)

    def test_count_inversions(self):
        """Test correctness of the count_inversions() function."""
        self.assertEqual(count_inversions([]), 0)
        self.assertEqual(count_inversions([1]), 0)
        self.assertEqual(count_inversions([5]), 0)
        self.assertEqual(count_inversions([1, 1]), 0)
        self.assertEqual(count_inversions([1, 2]), 0)
        self.assertEqual(count_inversions([2, 1]), 1)
        self.assertEqual(count_inversions([1, 2, 3]), 0)
        self.assertEqual(count_inversions([4, 5, 3]), 2)
        self.assertEqual(count_inversions([4, 6, 5]), 1)
        self.assertEqual(count_inversions([1, 1, 2, 2, 3]), 0)
        self.assertEqual(count_inversions([3, 1, 1, 2, 2]), 4)
        self.assertEqual(count_inversions([2, 4, 1, 3]), 3)
        self.assertEqual(count_inversions([2, 4, 1, 3, 5]), 3)
        self.assertEqual(count_inversions([8, 3, 5, 7, 6, 9]), 5)

    def test_provided_tests(self):
        """Test count_inversions() with the task-provided tests."""
        self.assertEqual(count_inversions([1, 3, 5, 2, 4, 6]), 3)
        self.assertEqual(count_inversions([1, 5, 3, 2, 4]), 4)
        self.assertEqual(count_inversions([5, 4, 3, 2, 1]), 10)
        self.assertEqual(count_inversions([1, 6, 3, 2, 4, 5]), 5)
        self.assertEqual(count_inversions([9, 12, 3, 1, 6, 8, 2, 5, 14, 13,
                                           11, 7, 10, 4, 0]), 56)
        self.assertEqual(count_inversions([37, 7, 2, 14, 35, 47, 10, 24, 44,
                                           17, 34, 11, 16, 48, 1, 39, 6, 33,
                                           43, 26, 40, 4, 28, 5, 38, 41, 42,
                                           12, 13, 21, 29, 18, 3, 19, 0, 32,
                                           46, 27, 31, 25, 15, 36, 20, 8, 9,
                                           49, 22, 23, 30, 45]), 590)
        self.assertEqual(count_inversions([4, 80, 70, 23, 9, 60, 68, 27, 66,
                                           78, 12, 40, 52, 53, 44, 8, 49, 28,
                                           18, 46, 21, 39, 51, 7, 87, 99, 69,
                                           62, 84, 6, 79, 67, 14, 98, 83, 0,
                                           96, 5, 82, 10, 26, 48, 3, 2, 15,
                                           92, 11, 55, 63, 97, 43, 45, 81, 42,
                                           95, 20, 25, 74, 24, 72, 91, 35, 86,
                                           19, 75, 58, 71, 47, 76, 59, 64, 93,
                                           17, 50, 56, 94, 90, 89, 32, 37, 34,
                                           65, 1, 73, 41, 36, 57, 77, 30, 22,
                                           13, 29, 38, 16, 88, 61, 31, 85, 33,
                                           54]), 2372)

# Run unit tests or file test based on number of arguments.
if __name__ == '__main__':
    if len(sys.argv) == 1:
        unittest.main()
    else:
        with open(sys.argv[1]) as file_handle:
            numbers = [int(x) for x in file_handle]
            print count_inversions(numbers)
